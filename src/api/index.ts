import { AxiosResponse } from 'axios';
import { instance as axios } from './axiosConfig';

type RequestShuffleTheCards = {
  success: boolean;
  deck_id: string;
  shuffled: boolean;
  remaining: number;
};
export type RequestCard = {
  image: string;
  value: string;
  suit: string;
  code: string;
};
export type DrawCard = {
  success: boolean;
  deck_id: string;
  remaining: number;
  cards: RequestCard[];
};

const shuffleTheCards = (id: string): Promise<AxiosResponse<RequestShuffleTheCards>> => {
  return axios.get(`/new/shuffle/?deck_count=${id}`);
};
const drawCard = (id: string, count: number): Promise<AxiosResponse<DrawCard>> => {
  return axios.get(`/${id}//draw/?count=${count}`);
};
const reshuffleTheCards = (id: string): Promise<AxiosResponse<RequestShuffleTheCards>> => {
  return axios.get(`${id}/shuffle/`);
};

export const app = {
  shuffleTheCards,
  drawCard,
  reshuffleTheCards,
};
