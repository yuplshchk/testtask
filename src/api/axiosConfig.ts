import { appSettings } from '../appSettings';
import axios from 'axios';

export const instance = axios.create({
  baseURL: appSettings.baseURL,
});

instance.interceptors.request.use(
  async (config) => {
    config.headers = {
      'Content-Type': 'application/json',
    };

    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

instance.interceptors.response.use(
  function (res) {
    let color = 34;
    res.status >= 200 && res.status < 300 && (color = 32);
    res.status >= 400 && res.status < 500 && (color = 31);
    res.status >= 500 && res.status < 600 && (color = 33);

    console.log(
      '\u001b[' + color + 'm' + `${res.status} -> baseURL${res.config.url}` + '\u001b[0m'
    );

    // Do something with response data
    return res;
  },
  function (error) {
    const res = error.response;
    let color = 34;
    res.status >= 200 && res.status < 300 && (color = 32);
    res.status >= 400 && res.status < 500 && (color = 31);
    res.status >= 500 && res.status < 600 && (color = 33);

    console.log(
      '\u001b[' + color + 'm' + `${res.status} -> baseURL${res.config.url}` + '\u001b[0m'
    );
    console.log('\u001b[' + color + 'm' + `${JSON.stringify(res.data)}` + '\u001b[0m');

    return Promise.reject(error);
  }
);
