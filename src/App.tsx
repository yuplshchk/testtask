import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Image } from 'react-native';
import { app, RequestCard } from './api';

const App = () => {
  const [remaining, setRemaining] = useState<number>(0);
  const [deskId, setDeskId] = useState<string>('');
  const [currentCard, setCurrentCard] = useState<Array<RequestCard>>([]);
  const [scope, setScope] = useState<number>(0);

  useEffect(() => {

    app.shuffleTheCards('1').then((res) => {
      setRemaining(res.data.remaining);
      setDeskId(res.data.deck_id);
    });
  }, []);

  const guess = ['higher', 'lower'];
  const random = Math.floor(Math.random() * guess.length);

  const getCard = () => {
    if (remaining !== 0) {
      app.drawCard(deskId, 1).then((res) => {
        setCurrentCard((prev) => {
          if (currentCard.length === 1) {
            if (res.data.cards[0].value < prev[0].value && random === 1) {
              setScope((prev) => prev + 1);
            } else if (res.data.cards[0].value > prev[0].value && random === 0) {
              setScope((prev) => prev + 1);
            }
          }
          return res.data.cards;
        });
        setRemaining(res.data.remaining);
      });
    }
  };
  const reshuffleTheCards = () => {
    app.reshuffleTheCards(deskId).then((res) => {
      setRemaining(res.data.remaining);
      setScope(0);
      setCurrentCard([]);
    });
  };
  return (
    <View style={styles.sectionContainer}>
      <Text style={styles.text}>Cards left the the desk {remaining}</Text>
      <View style={styles.container}>
        <TouchableOpacity onPress={() => getCard()} style={styles.card}>
          <Text style={styles.text}>Get Card</Text>
        </TouchableOpacity>
      </View>
        {currentCard.map((item: { image: any; code: any; }, index: any) => (
          <View style={styles.wrap}>
            <View style={styles.container}>
              <Image source={{ uri: item.image }} style={styles.images} key={item.code + index} />
            </View>
            <Text style={styles.text}>Guess next card will be {guess[random]}</Text>
          </View>
        ))}

      <View>
        <Text style={styles.text}>Scope</Text>
        <Text style={styles.text}>{scope}</Text>
      </View>
      {remaining === 0 && (
        <>
          <Text style={styles.textRed}>You are finished</Text>
          <TouchableOpacity onPress={() => reshuffleTheCards()} style={styles.button}>
            <Text style={styles.text}>Reshuffle the Cards</Text>
          </TouchableOpacity>
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    paddingTop: 32,
    paddingHorizontal: 24,
    backgroundColor: '#eee',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  wrap: {
    marginVertical: 10,
  },
  text: {
    textAlign: 'center',
  },
  textRed: {
    color: 'red',
    fontSize: 16,
    textAlign: 'center',
  },
  images: {
    width: 100,
    height: 150,
    resizeMode: 'contain',
    marginTop: 30,
    borderRadius: 5,
    marginRight: 5,
  },
  card: {
    width: 100,
    height: 150,
    backgroundColor: 'grey',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    marginTop: 30,
    borderRadius: 5,
  },
  button: {
    padding: 5,
    backgroundColor: 'red',
    borderRadius: 5,
    marginTop: 5,
  },
});

export default App;
